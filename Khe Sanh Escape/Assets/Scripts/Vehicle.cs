
using UnityEngine;
using System.Collections;
//including some .NET for dynamic arrays called List in C#
using System.Collections.Generic;

[RequireComponent(typeof(CharacterController))]


abstract public class Vehicle : MonoBehaviour {

	
	//vehicle attributes
	public float maxSpeed = 10.0f;
	public float maxForce = 10.0f;
	public float mass = 1.0f;
	public float radius = 0.5f;
	public float gravity = 20.0f;
	public int number;

	//variables for wander
	float wanderRad = 4.0f;
	float wanderDist = 5.0f;
	float wanderRand = 3.0f;
	float wanderAng = 0.0f;

	//variables for separation
	//list of nearby flockers
	private List<GameObject> nearFlockers = new List<GameObject> ();
	private List<float> nearFlockersDistances = new List<float> ();
	


	protected CharacterController characterController;
	protected Vector3 acceleration;	//change in velocity per second
	protected Vector3 velocity;		//change in position per second
	protected Vector3 dv;           //desired velocity
	public Vector3 Velocity {
		get { return velocity; }
		set { velocity = value;}
	}

	//Classes that extend Vehicle must override CalcSteeringForce
	abstract protected void CalcSteeringForce();

	virtual public void Start(){
		acceleration = Vector3.zero;
		characterController = gameObject.GetComponent<CharacterController> ();
	}

	
	// Update is called once per frame
	public virtual void Update () {
		CalcSteeringForce ();
		
		//update velocity
		velocity += acceleration * Time.deltaTime;
		velocity = Vector3.ClampMagnitude (velocity, maxSpeed);
		
		//orient the transform to face where we going
		if (velocity != Vector3.zero)
			transform.forward = velocity.normalized;
		
		// the CharacterController moves us subject to physical constraints
		characterController.Move (velocity * Time.deltaTime);
		
		//reset acceleration for next cycle
		acceleration = Vector3.zero;

	}

	protected void ApplyForce (Vector3 steeringForce){
		acceleration += steeringForce/mass;
	}

	
	//-------- functions that return steering forces -------------//

	//**************************************************************
	// Write this! It is included in the Vehicle class because it
	// likely to be used in other steering behaviors
	//**************************************************************
	protected Vector3 Separation (GameObject[] flockers, float close)
	{
		//empty our lists
		nearFlockers.Clear ();
		nearFlockersDistances.Clear ();
		float dist;
		Vector3 dv = Vector3.zero;
		for (int i = 0; i < flockers.Length; i++) {
			dist = Vector3.Distance(transform.position, flockers[i].transform.position);
			if(dist < close && dist != 0) {
				nearFlockers.Add(flockers[i]);
                nearFlockersDistances.Add (dist);
	         }
         }
        for (int j = 0; j < nearFlockers.Count; j++) {
					dv += Flee(nearFlockers[j].transform.position) / nearFlockersDistances[j];
		}
		dv = dv.normalized * maxSpeed; 	//scale by maxSpeed
		dv -= velocity;
		return dv;
	}


	// return the force to align with a given direction
	protected Vector3 Alignment (GameObject[] aligns){
		for (int i = 0; i < aligns.Length; i++)
		{
			dv += aligns[i].transform.forward;
		}
		dv.Normalize();
		dv *= maxSpeed;
		dv -= velocity;
		return dv;
	}

	protected Vector3 Cohesion (GameObject[] cohese){
		Vector3 avgPos = new Vector3 ();
		for (int i = 0; i < cohese.Length; i++)
		{
			avgPos += cohese[i].transform.position;
		}
		dv = Seek (avgPos);
		return dv;
	}


	protected Vector3 Seek (Vector3 targetPos)
	{
		//find dv, desired velocity
		dv = targetPos - transform.position;		
		dv = dv.normalized * maxSpeed; 	//scale by maxSpeed
		dv -= velocity;
		return dv;
	}

	protected Vector3 Arrive (Vector3 targetPos)
	{
		Vector3 offset = targetPos - transform.position;
		float dist = offset.magnitude;
		float ramp = maxSpeed * (dist / 75);
		float clip = Mathf.Min (ramp, maxSpeed);
		dv = (clip / dist) * offset;
		dv -= velocity;
		return dv;
	}
	
	protected Vector3 Flee (Vector3 targetPos)
	{
		//find dv, desired velocity
		dv = transform.position - targetPos;		
		dv = dv.normalized * maxSpeed; 	//scale by maxSpeed
		dv -= velocity;
		return dv;
	}

	//different take on Evade based on flight evasive maneuvers
	protected Vector3 Evade (Vector3 targetPos)
	{
		//find dv, desired velocity
		dv = transform.position - targetPos;		
		dv = dv.normalized * maxSpeed; 	//scale by maxSpeed
		dv -= velocity;
		dv += Wander ();
		return dv;
	}

	//**Avoid Obstacle not used, but its deletion caused strange errors, so it stays, commented out**


//	protected Vector3 AvoidObstacle (GameObject obst, float safeDistance)
//	{ 
//		dv = Vector3.zero;
//		float obRadius = obst.GetComponent<ObstacleScript> ().Radius; 
//		//safeDistance += radius + obRadius;
//
//		//vector from vehicle to center of obstacle
//		Vector3 vecToCenter = obst.transform.position - transform.position;
//		//eliminate y component so we have a 2D vector in the x, z plane
//
//		// distance should not be allowed to be zero or negative because 
//		// later we will divide by it and do not want to divide by zero
//		// or cause an inadvertent sign change.
//		float dist = Mathf.Max(vecToCenter.magnitude - obRadius - radius, 0.1f);
//		
//		// if too far to worry about, out of here
//		if (dist > safeDistance)
//			return Vector3.zero;
//		
//		//if behind us, out of here
//		if (Vector3.Dot (vecToCenter, transform.forward) < 0)
//			return Vector3.zero;
//		
//		float rightDotVTC = Vector3.Dot (vecToCenter, transform.right);
//		
//		//if we can pass safely, out of here
//		if (Mathf.Abs (rightDotVTC) > radius + obRadius)
//			return Vector3.zero;
//
//		//if we get this far, than we need to steer
//		
//		//obstacle is on right so we steer to left
//		if (rightDotVTC > 0)
//			dv = transform.right * -maxSpeed * safeDistance / dist;
//		else
//		//obstacle on left so we steer to right
//			dv = transform.right * maxSpeed * safeDistance / dist;
//
//		dv -= velocity;    //calculate the steering force
//		return dv;
//	}

	// smoothed random walk
	protected Vector3 Wander(){
		Vector3 target = transform.position + transform.forward * wanderDist;
		Quaternion rot = Quaternion.Euler(wanderAng*1.8f, wanderAng, wanderAng*1.5f);
		Vector3 offset = rot * transform.forward;
		target += offset * wanderRad;
		wanderAng += Random.Range (-wanderRand, wanderRand);
		return Seek (target);
	}

	protected void FloorCheck()
	{
		float tHeight = Terrain.activeTerrain.SampleHeight (transform.position) + 18;
		if (this.transform.position.y < tHeight && tHeight > 19)
		{
			Vector3 floor = new Vector3(transform.position.x, tHeight, this.transform.position.z);
			transform.position = floor;
		}
	}
}
