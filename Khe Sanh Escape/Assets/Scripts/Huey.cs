﻿using UnityEngine;
using System.Collections;

public class Huey : Vehicle {
	private GameObject[] MiGs;
	public float inFront;
	public float future;
	public float width;
	GameObject[] path;
	Vector3 target;
	Vector3 futurePosition;
	Vector3 closestPoint;
	float closestDistance;
	GameObject wp;
	public float seekWt = 75.0f;
	public float avoidWt = 60.0f;
	public float avoidDist = 20.0f;
	public float wanderWt = 10.0f;
	public float alignWt = 2.0f;
	public float clusterWt = 10.0f;
	public float sepWt = 8.0f;

	public bool run = false;
	public MiG chaser;
	public bool dead = false;
	
	public Vector3 centroid = Vector3.zero;
	public Vector3 rotAxis = Vector3.right;
	
	
	// Call Inerited Start and then do our own
	override public void Start () {
		base.Start();
		MiGs = GameObject.FindGameObjectsWithTag ("MiG");
		path = GameObject.FindGameObjectsWithTag ("WP");
		getClosestPoint ();
	}

	override public void Update()
	{
		ClosestMiG();
		base.Update ();

		//make sure the Huey doesnt tilt around
		if (velocity != Vector3.zero) 
		{
			Vector3 dir = velocity;
			dir.y = 0;
			transform.forward = dir.normalized;
		}
	}
	
	// All vehicles need to override CalcSteeringForce
	protected override void CalcSteeringForce(){
		Vector3 force = Vector3.zero;
		if (Time.deltaTime != 0.0f)
		MiGCheck();

		FloorCheck ();

		//path following
		nextClosestPoint ();
		target = closestPoint + wp.GetComponent<WayPoint>().unitVec * inFront;
		if (closestDistance > width/2) {
			force += seekWt * Seek (target);
		}

		//if someone has a lock on the Huey and isnt being chased away, run
		if (chaser != null) 
		{
			if (chaser.targeted == false && Vector3.Magnitude (transform.position - chaser.transform.position) < 100)
			{
				force += avoidWt * Flee (chaser.transform.position);
				//Debug.DrawLine(transform.position, chaser.transform.position,Color.blue);
			}
			if (chaser.targeted == true)
			{
				chaser = null;
			}
		}

		//dont let the MiGs get close
		for (int i = 0; i < MiGs.Length; i++) 
		{
			if (MiGs[i] != null && Vector3.Magnitude (transform.position - MiGs[i].transform.position) < 20)
			{
				Destroy (this.renderer);
				dead = true;
			}
		}

		//dont fall below y=23
		if (transform.position.y < 23 && force.y < 0) 
		{
			force.y = 0;
		}

		//containment
		if (Vector3.Magnitude (new Vector3 (250, 30, 250) - transform.position) > 350 || transform.position.y > 80) 
		{
			force = Seek(new Vector3(250, 30, 250));
		}
		//limit force to maxForce and apply
		force = Vector3.ClampMagnitude (force, maxForce);
		ApplyForce(force);
		
		//show force as a blue line pushing the guy like a jet stream
		//Debug.DrawLine(transform.position, transform.position - force,Color.blue);
		//red line to the target which may be out of sight
		//Debug.DrawLine (transform.position, target, Color.red);
	}

	//this and next method are for getting the next point on the path to try and reach
	private void nextClosestPoint(){
		GameObject[] nextPoints = new GameObject[2];
		nextPoints[0] = wp.gameObject;
		nextPoints [1] = nextPoints[0].GetComponent<WayPoint>().next;
		closestDistance = 1000;
		float curClosestDistance;
		Vector3 curClosestPoint;
		futurePosition = transform.position + velocity * future;
		for (int i=0; i < 2; i++) {
			GameObject curWP = nextPoints[i];
			curClosestPoint = curWP.GetComponent<WayPoint>().closestPoint(futurePosition);
			curClosestDistance = Vector3.Distance(curClosestPoint, futurePosition);
			if(curClosestDistance < closestDistance){
				closestDistance = curClosestDistance;
				closestPoint = curClosestPoint;
				wp = curWP;
			}
		}
	}
	
	private void getClosestPoint(){
		closestDistance = 1000;
		float curClosestDistance;
		Vector3 curClosestPoint;
		futurePosition = transform.position + velocity * future;
		for (int i=0; i < path.Length; i++) {
			GameObject curWP = path[i];
			curClosestPoint = curWP.GetComponent<WayPoint>().closestPoint(futurePosition);
			curClosestDistance = Vector3.Distance(curClosestPoint, futurePosition);
			if(curClosestDistance < closestDistance){
				closestDistance = curClosestDistance;
				closestPoint = curClosestPoint;
				wp = curWP;
			}
		}
	}

	private void ClosestMiG()
	{
		float dist = 10000;
		for (int i = 0; i < MiGs.Length; i++) 
		{
			if (MiGs[i] != null && Vector3.Magnitude (transform.position - MiGs[i].transform.position) < dist && MiGs[i].GetComponent<MiG>().targeted == false)
			{
				dist = Vector3.Magnitude (transform.position - MiGs[i].transform.position);
				chaser = MiGs[i].GetComponent<MiG>();
			}
		}
	}

	void MiGCheck()
	{
		for (int i = 0; i < MiGs.Length; i++) 
		{
			if (MiGs[i] != null && MiGs[i].GetComponent<MiG>().dead == true)
			{
				MiGs[i] = null;
			}
		}
	}
}

