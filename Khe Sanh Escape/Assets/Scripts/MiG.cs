﻿using UnityEngine;
using System.Collections;

public class MiG : Vehicle {
	public GameObject target;
	public F4 flee;
	//reference to an array of obstacle
	private GameObject[] F4s;
	// These weights will be exposed in the Inspector window
	public float seekWt = 75.0f;
	public float avoidWt = 10.0f;
	public float avoidDist = 20.0f;
	public float wanderWt = 10.0f;
	public float alignWt = 2.0f;
	public float clusterWt = 10.0f;
	public float sepWt = 8.0f;
	
	public bool run = false;
	public bool targeted = false;
	public bool dead = false;

	public Vector3 original;
	
	// Call Inerited Start and then do our own
	override public void Start () {
		base.Start();
		F4s = GameObject.FindGameObjectsWithTag ("F4");
		target = GameObject.FindGameObjectWithTag ("Huey");
		original = transform.position;
	}
	
	// All vehicles need to override CalcSteeringForce
	protected override void CalcSteeringForce(){
		Vector3 force = Vector3.zero;

		FloorCheck ();

		if (targeted == true) 
		{
			run = true;
		}
		
		//seek target if it is still alive
		if (!run && target.GetComponent<Huey>().dead == false) 
		{
			force += seekWt * Seek (target.transform.position);
		}

		//avoid F4s
		for (int i = 0; F4s.Length > i; i++) 
		{
			if (run == true)
			{
				force += avoidWt * Evade (flee.transform.position);
				if (Vector3.Magnitude(transform.position - flee.transform.position) > 200)
				{
					run = false;
					targeted = false;
					F4s[i].GetComponent<F4>().prey = null;
					F4s[i].GetComponent<F4>().track = true;
					F4s[i].GetComponent<F4>().hunt = false;
				}

			}
		}

		if (target.GetComponent<Huey> ().dead == true) 
		{
			force = seekWt * Wander ();
		}

		//"floor"
		if (transform.position.y < 23 && force.y < 0) 
		{
			force.y = 5;
		}

		if (Vector3.Magnitude (new Vector3 (250, 30, 250) - transform.position) > 350 || transform.position.y > 80) 
		{
			force = Seek(new Vector3(250, 30, 250));
		}

		
		//limit force to maxForce and apply
		force = Vector3.ClampMagnitude (force, maxForce);
		ApplyForce(force);	

		//red line to the target which may be out of sight
		//Debug.DrawLine (transform.position, target.transform.position,Color.red);
		
		
	}
}

