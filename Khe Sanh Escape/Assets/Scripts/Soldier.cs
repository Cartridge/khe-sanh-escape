﻿using UnityEngine;
using System.Collections;

public class Soldier : MonoBehaviour {
	public GameObject JGG;

	// Use this for initialization
	void Start () {
		JGG = GameObject.FindGameObjectWithTag ("Huey");
	}
	
	// Update is called once per frame
	void Update () {
		//"get picked up"
		if (Vector3.Magnitude (transform.position - JGG.transform.position) < 20) {
			this.renderer.enabled = false;
		} 

		//"new" soldiers arrive at the LZ
		else if (Vector3.Magnitude (transform.position - JGG.transform.position) > 200) {
			this.renderer.enabled = true;
		}
	}
}
