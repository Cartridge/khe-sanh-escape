﻿using UnityEngine;
using System.Collections;

// An Obstacle is a GameObject that we need to avoid
// all Obstacles should be able to return their radius
// and have an obstacle tag

//***Unity threw a fit when I tried to delete this, but it has no real funcion***
public class ObstacleScript : MonoBehaviour {

	public float radius = 1.414f; //hard coded or set in inspector for now

	// We will follow a convention that setters and getters, which
	// are functions, will have the same name as the variable they
	// reference, but with an upper-case initial letter.

	public float Radius {
		get { return radius; }
	}
	
	
}

	