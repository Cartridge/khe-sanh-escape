﻿using UnityEngine;
using System.Collections;

public class F4 : Vehicle {
	//group of important objects for decing behaviours
	public GameObject target;
	public MiG prey;
	private GameObject[] MiGs;
	private GameObject[] F4s;
	// These weights will be exposed in the Inspector window
	public float seekWt = 75.0f;
	public float avoidWt = 80.0f;
	public float avoidDist = 20.0f;
	public float wanderWt = 10.0f;
	public float alignWt = 2.0f;
	public float clusterWt = 10.0f;
	public float sepWt = 8.0f;

	//decides how to move
	public bool run = false;
	public bool targeted = false;
	public bool track = true;
	public bool hunt = false;
	public bool dead = false;

	
	// Call Inerited Start and then do our own
	override public void Start () {
		base.Start();

		//set position
		transform.position = new Vector3 (Random.Range (350, 500), Random.Range (30, 80), Random.Range (0, 30));

		//assignments
		MiGs = GameObject.FindGameObjectsWithTag ("MiG");
		F4s = GameObject.FindGameObjectsWithTag ("F4");
		target = GameObject.FindGameObjectWithTag ("Huey");

		maxForce = 5;
		maxSpeed = 10;
	}
	
	// Calculate how to move based on the group of bools
	protected override void CalcSteeringForce(){
		Vector3 force = Vector3.zero;

		FloorCheck ();

		HueyCheck ();

		//hunting MiGs
		for (int i = 0; MiGs.Length > i; i++) 
		{
			if (MiGs[i].GetComponent<MiG>().dead == false && target != null)
			{
			if (Vector3.Magnitude (target.GetComponent<Huey>().transform.position - MiGs[i].transform.position) < 75 && MiGs[i].GetComponent<MiG>().targeted == false && prey == null)
			{
				MiGs[i].GetComponent<MiG>().targeted = true;
				track = false;
				hunt = true;
				prey = MiGs[i].GetComponent<MiG>();
				MiGs[i].GetComponent<MiG>().flee = this;
			}
			if (prey != null && prey.flee != this)
			{
				track = true;
				hunt = false;
				prey = null;
			}
			}
		}

		//if withing 40 units of the MiG, destoy it and return to following the Huey
		if (prey != null && Vector3.Magnitude (transform.position - prey.transform.position) < 40)
		{
			prey.transform.position = prey.original;
			prey = null;
			track = true;
			hunt = false;
		}

		//seek Huey
		if (track) 
		{
			if (Vector3.Magnitude (transform.position - target.transform.position) > 40 && 
			    Vector3.Dot (target.transform.position - transform.position, transform.forward) > 0)
			{
				force += seekWt * Arrive (target.transform.position);
			}
			else
			{
				force += avoidWt * Flee (target.transform.position);
			}
			force += sepWt * Separation (F4s, 200.0f);
			force += clusterWt * Cohesion (F4s);
		}

		//seek MiG
		if (hunt) 
		{
			force += seekWt * Seek (prey.transform.position);
			//Debug.DrawLine (transform.position, prey.transform.position, Color.green);
		}

		//"floor"
		if (transform.position.y < 23) 
		{
			force.y = 5;
		}

		if (Vector3.Magnitude (new Vector3 (250, 30, 250) - transform.position) > 350 || transform.position.y > 80) 
		{
			force = Seek(new Vector3(250, 30, 250));
		}
		
		
		//limit force to maxForce and apply
		force = Vector3.ClampMagnitude (force, maxForce);

		ApplyForce(force);	
	}

	//if the huey is dead, the F4s have failed and die
	void HueyCheck()
	{
		if (target.GetComponent<Huey>().dead == true) 
		{
			Destroy (this.renderer);
			if (prey != null)
			{
				prey.run = false;
				prey.flee = null;
				prey.targeted = false;
			}
			dead = true;
		}
	}
}

