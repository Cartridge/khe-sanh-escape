June 12, 1968

We have to go back in there today. Previous patrol on the regular route thought they saw some of the 
9th Marines in the thick of vegetation. Radios told them to hold the top of the hill until we can rescue 
them. Two at a time, it's going to take forever to get them out... 

Command put a lot into this effort; right now they are fueling up some F4s to convoy. I thought they 
couldn't go this slow, but the SFC said that it would be fine, they might stall and stammer a bit though.
Their orders are to follow us unless we are in danger from enemy personnel. Our orders are to retreat,
and I think I overhead that they are to engage directly. 

God I'm so scared. I hate going out there, so close to the tree line. No room to move, too close to Charlie
below. Wouldn't take more than a rocket or a few well-placed shots to end this whole mission. I just want
this damn war to end so I can go home. A few more weeks, and this tour will end. God help me if I have to
do another. 

If someone finds this diary, please have it returned to my family. The SFC will know what to do. 

PFC Vince McMahon


(I modelled my world after Vietnam and the extraction efforts near the end of the Battle of Khe Sanh.
I made all of the aircraft models myself. I could not, however, find good enough reference for the F4, 
so I instead used the F22, which I had made previously for a modelling challenge. For reference, the wood
block in the dirt is supposed to be a the base where the Huey is taking the Marines. 

I have the Huey, which path follows unless being chased by a MiG. The F4s will, if not currently chasing 
a MiG, leader follow (one of my advanced movements) the Huey. While following the Huey, they will flock, 
but will disregard each other when chasing their prey. The MiG will Seek the Huey, unless targeted,
in which case it will Evade. I did Evade a little different than normal. Standard Evade was not producing
the effect I wanted, so instead my Evade is a combination of Flee and Wander (my second advanced movement), in an attempt to replicate
the weaving of a plane to escape targeting. All will Seek the center of the map if they stray more than
350 (250rt(2)) units, or if they reach 80 units high, the effective "ceiling" of the map. 

The "soldiers" are the "Guy" models we have been using for the past homeworks. The ones on the hill
will disappear (loading in) when the Huey gets close, and reappear (more Marines coming from the trees)
when it gets far enough away, effectively giving the Huey a reason to keep cycling along the path. )